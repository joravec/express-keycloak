## keycloak-express

A simple express server connected to ben's keycloak server on devci.

Do the usual npm install.

Run with npm start (or node ./bin/www).

The /logout endpoint will log the user out and redirect to /.

The /users endpoint will redirect to the keycloak login page. After logging in (joeo/joeo), the site redirects to the /users view.